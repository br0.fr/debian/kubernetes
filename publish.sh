#!/bin/sh

set -u
set -e

REPOSITORY="kubernetes-${CI_COMMIT_BRANCH}"
UPLOAD_DIR="${REPOSITORY}-$(date '+%s')"

CURL_FILES_ARGS=""

curl() {
  echo "/usr/bin/curl -s ${*}"
  /usr/bin/curl -s "${@}"
  echo
}

for f in packages/*.deb
do
  filename=$(basename "${f}")
  CURL_FILES_ARGS="${CURL_FILES_ARGS} -F ${filename}=@${f}"
done

if [ -z "${CURL_FILES_ARGS}" ]
then
  echo "No .deb files found in the 'packages' directory."
  exit 1
fi

curl -X POST ${CURL_FILES_ARGS} "${APTLY_URL}/files/${UPLOAD_DIR}"
curl -X POST "${APTLY_URL}/repos/${REPOSITORY}/file/${UPLOAD_DIR}?noRemove=0"
curl -X DELETE "${APTLY_URL}/files/${UPLOAD_DIR}"
curl -X PUT -H 'Content-Type: application/json' --data '{"Signing": {"Batch": true, "GpgKey": "'"${GPG_KEY}"'", "Passphrase": "'"${GPG_PASSPHRASE}"'"}}' "${APTLY_URL}/publish/:./kubernetes"
