INSTALL = install

VERSION = [CHANGE ME]
ARCHIVE_CHECKSUM = [CHANGE ME]
ARCHIVE_NAME = kubernetes-server-linux-amd64.tar.gz
ARCHIVE_URL = https://dl.k8s.io/v${VERSION}/${ARCHIVE_NAME}

build:
	mkdir -p build/
	curl -s -L ${ARCHIVE_URL} -C - -o build/${ARCHIVE_NAME}
	echo "${ARCHIVE_CHECKSUM}  build/${ARCHIVE_NAME}" | sha512sum --quiet -c -
	tar -x -f build/${ARCHIVE_NAME} -C build/

install:
	${INSTALL} -d ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kube-scheduler ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kube-apiserver ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kubectl ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kube-proxy ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kubelet ${DESTDIR}/bin
	${INSTALL} -m 755 build/kubernetes/server/bin/kube-controller-manager ${DESTDIR}/bin

clean:
	rm -rf build/
