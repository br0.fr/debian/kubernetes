# Debian packages for Kubernetes

This repository contains build configuration to create Debian packages from
Kubernetes official release binaries.

## Packages

### `kubernetes-common`

Contains common files such as `/etc/default/kubernetes` and post install/post
remove scripts.

### `kubectl`

Contains the `kubectl` binary.

### `kubernetes-controller`

Contains binaries needed for a controller host (`kube-apiserver`,
`kube-controller-manager`, `kube-scheduler`) and their systemd service file.

### `kubernetes-worker`

Contains binaries needed for a worker host (`kubelet`, `kube-proxy`) and their
systemd service file.

## Building

Install build dependencies (with [mk-build-deps][1] for instance) then run
`debuild -b -us -uc`.

Packages should build on any x86_64 distribution with debhelper >=12 available.

[1]: https://manpages.debian.org/buster/devscripts/mk-build-deps.1.en.htm

## Checking for new upstream versions

Information about new Kubernetes releases can be found in the CHANGELOG files
in their git repository. `uscan` and the `debian/watch` file allow for new
minor releases discovery:

```
uscan --no-download
```

## Hosted repository

If you don’t want to build these packages yourself, you can find them at
<https://apt.br0.fr>.

To use this repository, simply download the GPG key and add a `sources.list`
entry to your confiruation (replace `<major.minor>` with the version of your
choice):

```
$ wget -q https://apt.br0.fr/pubkey.gpg -O - | gpg --dearmor | sudo tee /usr/share/keyrings/apt.br0.fr-keyring.gpg >/dev/null
$ sudo echo "deb [signed-by=/usr/share/keyrings/apt.br0.fr-keyring.gpg] https://apt.br0.fr/ kubernetes <major.minor>" > /etc/apt/sources.list.d/kubernetes.list
```

Availability of this service is not guaranteed.

## Contributing

Every contributions are welcome.

Participation in this project is subject to a [code of
conduct](CODE_OF_CONDUCT.md).
